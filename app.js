var app = angular.module('WorldClock', ['ServiceUser','ngRoute','base64', 'ui.bootstrap', 'angular-growl', 'ngAnimate', 'angularjs-datetime-picker', 'ngTable', 'ngCookies']);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.timeout = 10000;
}]);

app.config(['growlProvider', function(growlProvider) {
    growlProvider.globalPosition('top-right');
    growlProvider.globalTimeToLive({success: 2000, error: 4000})
}]);

app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "pages/login_page.html"
        })
        .when("/userpage/:id", {
            controller : 'UserController',
            templateUrl : "pages/user_page.html"
        })
        .when("/allusers", {
            controller : 'AllUserController',
            templateUrl : "pages/all_users.html"
        });
});

app.factory('ConfigurationRepository', function () {

    var data = {
        BACKEND_HOSTNAME : 'localhost:8080'
    };

    return {
        getBackendHostName : function(){
            return data.BACKEND_HOSTNAME;
        }
    }
});

app.factory('UserObj', function() {
    var userData = {
        id : null,
        first_name : null,
        last_name : null,
        username: null,
        token: null,
        role: null,
        jogs : []
    };

    return {
        setState: function(id, first_name, last_name, username, token, role, jogs) {
            userData.id = id;
            userData.first_name = first_name;
            userData.last_name = last_name;
            userData.token = token;
            userData.username = username;
            userData.role = role;
            userData.jogs = jogs;
        },
        updateToken : function(token){
            userData.token = token;
        },
        updateUsername : function(username){
            userData.username = username;
        },
        getData: function() { return userData }
    };
});

app.directive('user', function() {
    return {
        restrict: 'E',
        templateUrl: 'templates/user-template.html'
    };
});

app.directive("loginForm", function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/login-template.html',
        replace: 'true'
    }
});


app.directive("registerForm", function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/register-template.html',
        replace: 'true'
    }
});

app.directive("navBar", function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/nav-bar-template.html',
        replace: 'true'
    }
});

