app.controller('AllUserController', function ($scope,  $location, AuthorizedHttp, ConfigurationRepository, UserService, UserObj) {


    $scope.logged_in_user = null;

    $scope.init = function () {
        $scope.logged_in_user = UserObj.getData().id;
        UserService.getAllUsers()
            .success(function (response) {
                $scope.users = response.users
            })
            .error(function (err, status) {
                alert('Error loading all users ')
            });
    };

});