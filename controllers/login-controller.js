var app = angular.module("WorldClock");

app.controller("LoginController", function ($scope, $cookies, $window, AuthorizedHttp, ConfigurationRepository, UserObj) {

    $scope.loginError = false;
    $scope.login = function () {
        $scope.loginError = false;
        UserObj.setState(null, null, null, $scope.username, null, null);
        AuthorizedHttp.getWithBasicAuth('http://{0}/login'.format(ConfigurationRepository.getBackendHostName()), null, $scope.username, $scope.password)
            .then(function successCallback(response) {
                UserObj.setState(response.data.id, response.data.firstName, response.data.lastName, response.data.username, response.headers("x-auth-token"), response.data.role, response.data.jogs);
                $scope.logged_in_user = response.data.id;

                $cookies.put('user_id', response.data.id);
                $cookies.put('session_id', response.headers("x-auth-token"));

                $scope.customNavigate('userpage', response.data.id);
            }, function errorCallback(response) {
                $scope.username = '';
                $scope.password = '';
                $scope.loginError = true;
            });
    }
});