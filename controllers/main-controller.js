var app = angular.module("WorldClock");

app.controller("MainController", function ($scope, $location) {

    $scope.customNavigate = function(routeToNavigate, routeParameter) {
        $location.path("/" + routeToNavigate + "/" + routeParameter);
    };


    $scope.navigateToRoot = function() {
        $location.path("/");
    }

});