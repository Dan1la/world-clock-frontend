var app = angular.module("WorldClock");

app.controller("NavBarController", function ($scope, $location, AuthorizedHttp, ConfigurationRepository, UserObj) {


    $scope.logged_in_user = UserObj.getData().id;
    $scope.logged_in_user_first_name = UserObj.getData().first_name;
    $scope.logged_in_user_last_name= UserObj.getData().last_name;

    $scope.logged_in_user_role = UserObj.getData().role;


    $scope.logout = function(){
        UserObj.setState(null, null, null, null, null, null);
        $scope.navigateToRoot()
    }
});