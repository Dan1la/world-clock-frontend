app.controller('RegisterController', function ($scope, $http, AuthorizedHttp, ConfigurationRepository, NotificationService) {

    $scope.registerationError = false;
    $scope.errMsg = '';
    $scope.register = function () {

        if($scope.registerForm.$valid) {
            $scope.registerationError = false;
            AuthorizedHttp.post('http://{0}/signup'.format(ConfigurationRepository.getBackendHostName()), {
                first_name: $scope.first_name,
                last_name: $scope.last_name,
                user_name: $scope.email,
                password: $scope.password,
                isEnabled: true
            }).then(function successCallback(response) {
                NotificationService.displaySuccess('Successfully registered.')
            }, function errorCallback(response, status) {
                if(response.status == 409) {
                    $scope.errMsg = "Sorry. Email already registered.";
                    NotificationService.displayError($scope.errMsg);
                }
                $scope.email = '';
                $scope.password = '';
                $scope.registerationError = true;
            });
        }
        else{
            $scope.registerForm.submitted = true;
        }
    }
});