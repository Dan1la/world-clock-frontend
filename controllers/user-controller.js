var app = angular.module("WorldClock");

app.controller('UserController', function ($scope, $cookies, $routeParams, $route, AuthorizedHttp, ConfigurationRepository, UserObj, UserService, NotificationService, NgTableParams) {

    //autoLogin();

    $scope.logged_in_user = UserObj.getData().id;
    $scope.logged_in_user_role = UserObj.getData().role;
    $scope.allRoles = ['USER', 'USER_MANAGER', 'ADMIN'];
    $scope.date_from = moment().format('YYYY-MM-DD HH:mm:ss');
    $scope.date_to = moment().format('YYYY-MM-DD HH:mm:ss');
    $scope.days = 0;
    $scope.hours = 0;
    $scope.minutes = 0;
    $scope.seconds = 0;


    getUser($routeParams.id);

    // function autoLogin(){
    //     var token = $cookies.get('session_id');
    //     var user_id = $cookies.get('user_id');
    //     if(token) {
    //         UserObj.setState(user_id, null, null, null, token, null, null);
    //         UserService.getMe()
    //             .then(function successCallback(response) {
    //                 $scope.current_user = response.data;
    //                 UserObj.setState(response.data.id, response.data.firstName, response.data.lastName, response.data.username, token, response.data.role, response.data.jogs);
    //             }, function errorCallback(response) {
    //                 NotificationService.displayError('Failed to refresh a session.');
    //             });
    //     }
    // }

     function getUser (id) {
        UserService.getUser(id)
            .then(function (response) {
                $scope.current_user = response.data;
                refreshUser(response.data);
                update_jog_table($scope.user_jogs);
                getWeeklyReports($scope.current_user.id);
            });
     }

     function getWeeklyReports(user_id) {
         UserService.getWeeklyReport(user_id)
             .then(function successCallback(response) {
                 populate_weekly_grid(response.data.weeklyStats.map(function(item){return constructWeeklyReportObject(item)}));
                 NotificationService.displaySuccess('Weekly report updated.');
             }, function errorCallback(response) {
                 NotificationService.displayError('Failed to update weekly report.');
             });
     }

     $scope.query_jogs = function(date_from, date_to){
        UserService.queryJog($scope.current_user.id, date_from, date_to, 1000)
            .then(function successCallback(response) {
                var jogs = response.data.jogs.map(function (item) {
                    return constructJogObject(item)
                });
                populate_jog_grid(jogs);
                NotificationService.displaySuccess('Jogs filtered.');
            }, function errorCallback(response) {
                NotificationService.displayError('Failed to filter.');
            });
     };

    $scope.add_jog = function () {
        UserService.addJog($scope.current_user.id, $scope.date_time, convertDaysHoursMinutesToMillis($scope.days, $scope.hours, $scope.minutes, $scope.seconds), $scope.distance)
            .success(function (response) {
                var jog = constructJogObject(response);
                $scope.user_jogs.push(jog);
                UserObj.getData().jogs.push(jog);
                update_jog_table($scope.user_jogs);
                getWeeklyReports($scope.current_user.id);
            })
            .error(function (err, status) {
                if(err != null && err.message != null) {
                    NotificationService.displayError(err.message)
                }
            });
    };

    $scope.update_jog = function(jog){
        UserService.updateJog($scope.current_user.id, jog)
            .then(function successCallback(response) {
                NotificationService.displaySuccess('Jog updated successfully.');
                getUser($scope.current_user.id)
            }, function errorCallback(response) {
                if(response.status == 403){
                    NotificationService.displayError('Access denied.')
                }else{
                    NotificationService.displayError('Failed to update jog.')
                }
            });
    };

    $scope.delete_jog = function(jog, index){
        UserService.deleteJog($scope.current_user.id, jog.id)
            .then(function successCallback(response) {
                $scope.user_jogs.splice(index, 1);
                update_jog_table($scope.user_jogs);
                getWeeklyReports($scope.current_user.id);
                NotificationService.displaySuccess('Jog deleted successfully.')
            }, function errorCallback(response) {
                if(response.status == 403){
                    NotificationService.displayError('Access denied.')
                }else {
                    NotificationService.displayError('Failed to delete jog.')
                }
            });
    };

    $scope.updateCurrentUser = function () {

        if($scope.current_user.username == UserObj.getData().username) {
            UserService.updateUserDetailsBasicAuth($scope.current_user.id, $scope.current_user_first_name, $scope.current_user_last_name, $scope.current_user_email, $scope.current_user_new_password, $scope.current_user_password)
                .then(function successCallback(response) {
                    UserService.login($scope.current_user_email, $scope.current_user_new_password ? $scope.current_user_new_password : $scope.current_user_password)
                        .then(function successCallback(response) {
                            $scope.current_user = response.data;
                            UserObj.setState(response.data.id, response.data.firstName, response.data.lastName, response.data.username, response.headers("x-auth-token"), response.data.role, response.data.jogs);
                            $scope.current_user.username = $scope.current_user_email;
                            $scope.current_user_new_password = "";
                            $scope.current_user_password = "";
                        }, function errorCallback(response) {
                        });
                    NotificationService.displaySuccess('User details successfully updated.')
                }, function errorCallback(response) {
                    if(response.status == 403){
                        NotificationService.displayError('Access denied.')
                    }else{
                        NotificationService.displayError('Failed to update user details.')
                    }
                });
        }
        else{
            UserService.updateUserDetails($scope.current_user.id, $scope.current_user_first_name, $scope.current_user_last_name, $scope.current_user_email, $scope.current_user_new_password)
                .then(function successCallback(response) {
                    $scope.current_user = response.data;
                    $scope.current_user.username = $scope.current_user_email;
                    $scope.current_user_new_password = "";
                    $scope.current_user_password = "";
                    NotificationService.displaySuccess('User details successfully updated.')
                }, function errorCallback(response) {
                    if(response.status == 403){
                        NotificationService.displayError('Access denied.')
                    }else{
                        NotificationService.displayError('Failed to update user details.')
                    }
                });
        }
    };


    $scope.deleteCurrentUser = function () {
        UserService.deleteUser($scope.current_user.id)
            .then(function successCallback(response) {
                if($scope.current_user.username == UserObj.getData().username) {
                    $scope.navigateToRoot();

                }else{
                    $scope.customNavigate('allusers', '');
                }
                NotificationService.displaySuccess('User {0} successfully deleted.'.format($scope.current_user.username))
            }, function errorCallback(response) {
                if(response.status == 403){
                    NotificationService.displayError('Access denied.')
                }else{
                    NotificationService.displayError('Failed to delete user.')
                }
            });
    };

    $scope.updateCurrentUserRole = function(new_role){
        UserService.updateUserRole($scope.current_user.id, new_role)
            .then(function successCallback(response) {
                $scope.current_user.role = new_role;
                NotificationService.displaySuccess('Role successfully updated.')
            }, function errorCallback(response) {
                if(response.status == 403){
                    NotificationService.displayError('Access denied.')
                }else{
                    NotificationService.displayError('Failed to update user role.')
                }
            });
    };

    var populate_weekly_grid = function (weekly_report) {
        $scope.weeklyGrid = new NgTableParams({count: 5}, {data: weekly_report});
    };

    var populate_jog_grid = function (jogs) {
        $scope.tableParams = new NgTableParams({count: 5}, {data: jogs});
    };

    var refreshUser = function(response){
        $scope.current_user_first_name = response.firstName;
        $scope.current_user_last_name = response.lastName;
        $scope.current_user_email = response.username;
        $scope.current_user_password = "";
        $scope.user_jogs = response.jogs.map(function (item) {
            return constructJogObject(item)
        });
        $scope.current_user_role = response.role;

        $scope.current_user.first_name = response.firstName;
        $scope.current_user.last_name = response.lastName;
        $scope.current_user.user_name = response.username;
        $scope.current_user.role = response.role;

    };

    var constructJogObject = function(jog){
        var time = dhm(jog.durationMillis);

        return{
            'id' : jog.id,
            'date' : jog.date,
            'distance_meters' : jog.distanceMeters,
            'duration_days' : time.days,
            'duration_hours' : time.hours,
            'duration_minutes' : time.minutes,
            'duration_seconds' : time.seconds,
            'avg_speed_km_per_hour' : parseFloat(0.060 * jog.avgSpeedMetersPerMinute).toFixed(2)
        }
    };


    var constructWeeklyReportObject = function (weeklyReport) {

        return {
            'week_date_from' : weeklyReport.week_date_from,
            'avg_distance_meters' : weeklyReport.avgDistanceMeters,
            'avg_speed_km_per_hour' : parseFloat(0.060 * weeklyReport.avgSpeedMetersPerMinute).toFixed(2)
        }

    };

    var update_jog_table = function (jogs) {
        $scope.tableParams = new NgTableParams({count: 5}, {data: jogs});
    };


    function dhm(ms) {
        var d, h, m, s;
        s = Math.floor(ms / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;
        return { "days": d, "hours": h, "minutes": m, "seconds": s };
    };

    function convertDaysHoursMinutesToMillis (days, hours, minutes, seconds){
        days = days ? days : 0;
        hours = hours ? hours : 0;
        minutes = minutes ? minutes : 0;
        seconds = seconds ? seconds : 0;
        return ((days * 24) * 60 * 60 * 1000) + (hours * 60 * 60 * 1000) + (minutes * 60 * 1000) + (seconds * 1000);
    }

});