var app = angular.module("WorldClock");


app.directive('worldClock', function($compile, $interval) {
    return {
        restrict: 'E',
        scope: {
            timezone: "="
        },
        link: function(scope, element) {

            var tick = function() {
                scope.time = moment().tz(scope.timezone).format('hh:mm:ss a');
                scope.gtm_diff = moment().tz(scope.timezone).format('Z');
                scope.timezone_code = moment().tz(scope.timezone).format('z');

            };
            tick();
            $interval(tick, 1000);
            $compile(element.contents())(scope);
        }
    };
});
