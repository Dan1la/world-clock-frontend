app.factory('AuthorizedHttp', function ($http, $base64, UserObj) {

    var authorizedHttpService = {

        getWithBasicAuth : function(url, params, username, password){
            return $http.get(url, Object.assign({headers: userBasicAuthHeader(username, password)}, {params: params}))
        },
        get: function (url, params) {
            return $http.get(url, Object.assign({headers: userXAuthHeader()}, {params: params}));
        },
        post: function (url, data, config) {
            return $http.post(url, data, Object.assign({headers: userXAuthHeader()}, config));
        },
        patchWithBasicAuth: function (url, data, config, password) {
            return $http.patch(url, data, Object.assign({headers: userBasicAuthHeader(UserObj.getData().username, password)}, config));
        },
        put: function (url, data, config) {
            return $http.put(url, data, Object.assign({headers: userXAuthHeader()}, config));
        },
        delete: function (url) {
            return $http.delete(url, Object.assign({headers: userXAuthHeader()}));
        },
        patch : function(url, data, config){
            return $http.patch(url, data, Object.assign({headers: userXAuthHeader()}, config));
        },
        deleteWithBody : function(url, data){
            var config = {
                method: "DELETE",
                url: url,
                data: data,
                headers: {"Content-Type": "application/json;charset=utf-8",
                    "x-auth-token" : UserObj.getData().token}
            };
            return $http(config);
        },
        basicAuthHeader: function (username, password) {
            var auth = $base64.encode("{0}:{1}".format(username, password));
            return {"Authorization": "Basic " + auth};
        },
        xAuthTokenHeader : function (xAuthToken) {
            var auth = $base64.encode("{0}".format(xAuthToken));
            return {"x-auth-token" : xAuthToken};
        }
    };

    var userXAuthHeader = function () {
        return authorizedHttpService.xAuthTokenHeader(UserObj.getData().token);
    };

    var userBasicAuthHeader = function(username, password) {
        return authorizedHttpService.basicAuthHeader(username, password)
    };
    return authorizedHttpService

});