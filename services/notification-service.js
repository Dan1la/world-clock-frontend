app.factory('NotificationService', function (growl, UserObj) {

    return {
        displaySuccess: function (msg) {
            return growl.success(msg, {disableCountDown: true});
        },
        displayError: function (msg) {
            return growl.error(msg, {disableCountDown: true})
        }
    };
});