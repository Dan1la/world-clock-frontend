 var ServiceUser = angular.module('ServiceUser', [])
     .service('UserService', function (AuthorizedHttp, ConfigurationRepository, UserObj) {

         this.login = function(username, password){
             return AuthorizedHttp.getWithBasicAuth('http://{0}/login'.format(ConfigurationRepository.getBackendHostName()), null, username, password);
         };

        this.getUser = function (id) {
            return AuthorizedHttp.get('http://{0}/users/{1}'.format(ConfigurationRepository.getBackendHostName(), id));
        };
         this.getMe = function () {
             return AuthorizedHttp.get('http://{0}/users/me'.format(ConfigurationRepository.getBackendHostName()));
         };
        this.getAllUsers = function () {
            return AuthorizedHttp.get('http://{0}/users'.format(ConfigurationRepository.getBackendHostName()));
        };

        this.updateUserDetails = function(idToUpdate, first_name, last_name, email, password){

            var data = JSON.stringify({
                first_name: first_name,
                last_name: last_name,
                user_name: email,
                password: password,
                isEnabled: true
            });
            return AuthorizedHttp.patch('http://{0}/users/{1}'.format(ConfigurationRepository.getBackendHostName(), idToUpdate), data)
        };

        this.getWeeklyReport = function(id){
            return AuthorizedHttp.get('http://{0}/users/{1}/weekly'.format(ConfigurationRepository.getBackendHostName(), id));
        };

         this.updateUserDetailsBasicAuth = function(idToUpdate, new_first_name, new_last_name, new_email, new_password, current_password){

             var data = JSON.stringify({
                 first_name: new_first_name,
                 last_name: new_last_name,
                 user_name: new_email,
                 password: new_password,
                 isEnabled: true
             });
             return AuthorizedHttp.patchWithBasicAuth('http://{0}/users/{1}'.format(ConfigurationRepository.getBackendHostName(), idToUpdate), data, null, current_password);
         };

        this.deleteUser = function (user_to_delete) {
            return AuthorizedHttp.delete('http://{0}/users/{1}'.format(ConfigurationRepository.getBackendHostName(), user_to_delete));
        };

        this.updateUserRole = function(user_id, role){
            var data = JSON.stringify({user_role : role});
            return AuthorizedHttp.patch('http://{0}/users/{1}/role'.format(ConfigurationRepository.getBackendHostName(), user_id), data);
        };

        this.addJog = function (user_id, date, duration_millis, distance_meters) {
            var data = {
                    'date' : date,
                    'distance_meters' : distance_meters,
                    'duration_millis' : duration_millis
                };
            return AuthorizedHttp.post('http://{0}/users/{1}/jog'.format(ConfigurationRepository.getBackendHostName(), user_id), data);
        };

        this.updateJog = function(user_id, jog){

            var data =  {
                'date' : jog.date,
                'distance_meters' : jog.distance_meters,
                'duration_millis': convertDaysHoursMinutesToMillis(jog.duration_days, jog.duration_hours, jog.duration_minutes, jog.duration_seconds)
            };
            return AuthorizedHttp.patch('http://{0}/users/{1}/jog/{2}'.format(ConfigurationRepository.getBackendHostName(), user_id, jog.id), data);
        };

         this.deleteJog = function (user_id, jog_id) {
             return AuthorizedHttp.delete('http://{0}/users/{1}/jog/{2}'.format(ConfigurationRepository.getBackendHostName(), user_id, jog_id));
         };

         this.queryJog = function(user_id, date_time_from, date_time_to, limit){
              return AuthorizedHttp.get('http://{0}/users/{1}/jog?date_time_from={2}&date_time_to={3}&limit={4}'
                  .format(ConfigurationRepository.getBackendHostName(), user_id, date_time_from, date_time_to, limit));
         };

         function convertDaysHoursMinutesToMillis (days, hours, minutes, seconds){
             days = days ? days : 0;
             hours = hours ? hours : 0;
             minutes = minutes ? minutes : 0;
             return ((days * 24) * 60 * 60 * 1000) + (hours * 60 * 60 * 1000) + (minutes * 60 * 1000) + (seconds * 1000);
         };

});